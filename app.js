const express = require("express");
const app = express();
const devcenterRoutes = require("./routes/devcenter");
const clientRoutes = require("./routes/client");
const locationRoutes = require("./routes/location");
const roomRoutes = require("./routes/room");
const bookingRoutes = require("./routes/booking");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
app.use(bodyParser.json());
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE");
  res.header("Access-Control-Allow-Headers", "Content-Type");
  next();
});
mongoose.connect("mongodb://localhost/conference-rooms-db");
var db = mongoose.connection;
db.on("error", console.error.bind(console, "MongoDB connection error:"));

app.use("/devcenters", devcenterRoutes);
app.use("/clients", clientRoutes);
app.use("/locations", locationRoutes);
app.use("/rooms", roomRoutes);
app.use("/bookings", bookingRoutes);
const port = process.env.PORT || 5000;

app.listen(port, () => console.log(`listening at ${port}`));
