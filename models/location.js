var mongoose = require("mongoose");

const locationSchema = mongoose.Schema({
  clientId: { type: String, required: true },
  location: { type: String, required: true }
});

module.exports = mongoose.model("Location", locationSchema);
