var mongoose = require("mongoose");

const roomSchema = mongoose.Schema({
  roomNumber: { type: Number, required: true },
  seatingCapacity: { type: Number, required: true },
  locationId: { type: String, required: true }
});

module.exports = mongoose.model("Room", roomSchema);
