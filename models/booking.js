var mongoose = require("mongoose");

const bookingSchema = mongoose.Schema({
  roomId: { type: String, required: true },
  slots: { type: [Number], required: true },
  date: { type: String, required: true }
});

module.exports = mongoose.model("Booking", bookingSchema);
