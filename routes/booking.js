const express = require("express");
const router = express.Router();
const BookingUtil = require("../utils/bookingUtil");

router.get("/:roomId/:date", (req, res, next) => {
  BookingUtil.getBookedSlots(
    { roomId: req.params.roomId, date: req.params.date },
    (err, doc) => {
      if (err) {
        console.log(err);
        res.status(500).json({
          error: err
        });
      }
      var slots = [];
      for (let i = 0; i < doc.length; i++) {
        slots = slots.concat(doc[i].slots);
      }
      res.status(200).json(slots);
    }
  );
});

router.post("/", (req, res) => {
  BookingUtil.checkBooking(
    { roomId: req.body.roomId, date: req.body.date },
    "slots",
    (err, doc) => {
      if (err) {
        console.log(err);
        res.status(500).json({
          error: err
        });
      } else if (doc.length >= 1) {
        console.log(doc);
        var slots = [];
        for (let i = 0; i < doc.length; i++) {
          slots = slots.concat(doc[i].slots);
        }
        var found = req.body.slots.some(s => slots.includes(s));
        if (found) {
          res.status(400).json({ message: "already booked" });
        } else {
          BookingUtil.addBooking(req.body, (err, doc) => {
            if (err) {
              console.log(err);
              res.status(500).json({
                error: err
              });
            }
            res.status(201).json({ message: "booking done" });
          });
        }
      } else {
        BookingUtil.addBooking(req.body, (err, doc) => {
          if (err) {
            console.log(err);
            res.status(500).json({
              error: err
            });
          }
          res.status(201).json({ message: "booking done" });
        });
      }
    }
  );
});
module.exports = router;
