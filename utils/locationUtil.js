const Location = require("../models/location");

module.exports.addLocation = function(query, callback) {
  Location.create(query, callback);
};
module.exports.checkLocation = function(query, callback) {
  Location.find(query, callback);
};

module.exports.getLocations = function(query, callback) {
  Location.find(query, callback);
};

module.exports.deleteLocation = function(query, callback) {
  Location.deleteOne(query, callback);
};

module.exports.updateLocation = function(query, update, callback) {
  Location.findOneAndUpdate(query, update, callback);
};
