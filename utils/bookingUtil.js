const Booking = require("../models/booking");
module.exports.addBooking = function(query, callback) {
  Booking.create(query, callback);
};

module.exports.checkBooking = function(query, fields, callback) {
  Booking.find(query, fields, callback);
};
module.exports.getBookedSlots = function(query, callback) {
  Booking.find(query, callback);
};
