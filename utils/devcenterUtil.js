const Devcenter = require("../models/devcenter");

module.exports.addDevcenter = function(query, callback) {
  Devcenter.create(query, callback);
};
module.exports.checkDevcenter = function(query, callback) {
  Devcenter.find(query, callback);
};

module.exports.getDevcenters = function(query, callback) {
  Devcenter.find(query, callback);
};

module.exports.deleteDevcenter = function(query, callback) {
  Devcenter.deleteOne(query, callback);
};

module.exports.updateDevcenter = function(query, update, callback) {
  Devcenter.findOneAndUpdate(query, update, callback);
};
